import React, { Component } from "react";
import InputUrl from "./InputUrl";
import SubmitButton from "./SubmitButton";
import PropTypes from "prop-types";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSubmitButtonEnabled: false,
      urlToCrawl: null
    };
  }

  onInputUrlValueChange(newValues) {
    this.setState({
      isSubmitButtonEnabled: newValues.showError,
      urlToCrawl: newValues.urlValue
    });
  }

  onButtonSubmit() {
    this.props.onFormSubmit(this.state.urlToCrawl);
  }

  render() {
    return (
      <div>
        <InputUrl
          onInputUrlValueChange={this.onInputUrlValueChange.bind(this)}
        />
        <SubmitButton
          isSubmitButtonEnabled={this.state.isSubmitButtonEnabled}
          onButtonSubmit={this.onButtonSubmit.bind(this)}
        />
      </div>
    );
  }
}

InputUrl.propTypes = {
  onFormSubmit: PropTypes.func
};

export default Form;
