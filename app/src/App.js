import React, { Component } from "react";
import Form from "./Form";
import CrawlResult from "./CrawlResult";

const BASE_API_URL = process.env.REACT_APP_BASE_API_URL;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      crawledData: null
    };
  }

  onFormSubmit(uri) {
    fetch(BASE_API_URL, {
      method: "POST",
      body: JSON.stringify({ uri: uri })
    })
      .then(res => res.text())
      .then(body => this.setState({ crawledData: body }))
      .catch(err => err);
  }

  render() {
    return (
      <section className="section">
        <div className="container">
          <h1 className="title">Crawl24 - Crawl your website</h1>
          <Form
            onFormSubmit={this.onFormSubmit.bind(this)}
            className="columns"
          />
          <CrawlResult crawledData={this.state.crawledData} />
        </div>
      </section>
    );
  }
}

export default App;
