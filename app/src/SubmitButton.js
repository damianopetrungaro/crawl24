import React from "react";
import PropTypes from "prop-types";

const SubmitButton = props => (
  <button
    type="button"
    disabled={props.isSubmitButtonEnabled === false}
    className={"button"}
    onClick={props.onButtonSubmit}
  >
    Crawl page!
  </button>
);

SubmitButton.propTypes = {
  onButtonSubmit: PropTypes.func
};

export default SubmitButton;
