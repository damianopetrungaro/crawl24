import React, { Component } from "react";
import PropTypes from "prop-types";

const URL_REGEX = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/gi;

class InputUrl extends Component {
  updateUrlValue = event => {
    this.setState({ urlValue: event.target.value }, this.validateInput);
  };

  constructor(props) {
    super(props);
    this.state = {
      urlValue: "",
      showError: false,
      isValid: false
    };
    this.updateUrlValue = this.updateUrlValue.bind(this);
  }

  validateInput() {
    this.state.showError = null === this.state.urlValue.match(URL_REGEX);
    this.props.onInputUrlValueChange({
      urlValue: this.state.urlValue,
      showError: !this.state.showError
    });
  }

  render() {
    return (
      <div className="field">
        <label className="label">Insert url to crawl</label>
        <div className="control">
          <input
            type="text"
            name="urlValue"
            autoComplete={"off"}
            value={this.state.urlValue}
            onInput={this.updateUrlValue}
            className={
              this.state.showError === true
                ? "input is-danger"
                : "input is-primary"
            }
          />
        </div>
        <p className="help">
          {this.state.showError === true ? "Please, insert a valid Url" : ""}
        </p>
      </div>
    );
  }
}

InputUrl.propTypes = {
  onInputUrlValueChange: PropTypes.func
};

export default InputUrl;
