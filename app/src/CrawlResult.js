import React from 'react'
import JSONPretty from 'react-json-pretty'

export default props => <JSONPretty json={props.crawledData}></JSONPretty>;
