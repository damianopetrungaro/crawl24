<?php

declare(strict_types=1);

use Crawl24\App\Controller\AnalyseWebPage;
use Dotenv\Dotenv;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

require __DIR__.'/../vendor/autoload.php';
$env = new Dotenv(__DIR__.'/../');
$env->load();

(function () {
    $container = require __DIR__.'/../bootstrap/container.php';
    $app = new \Slim\App($container);

    $app->get('/', function (RequestInterface $request, ResponseInterface $response) {
        $response->getBody()->write('Crawl 24 API');

        return $response;
    });

    $app->post('/', AnalyseWebPage::class);

    $app->add(function ($req, $res, $next) use ($container) {
        $response = $next($req, $res);

        return $response
            ->withHeader('Access-Control-Allow-Origin', $container->get('settings')['appUrl'])
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    });

    $app->run();
})();
