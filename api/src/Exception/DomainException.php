<?php

declare(strict_types=1);

namespace Crawl24\App\Exception;

use InvalidArgumentException;
use Throwable;

final class DomainException extends InvalidArgumentException
{
    /** @var mixed */
    private $additionalData;

    public function __construct(string $message = '', $additionalData = null, Throwable $previous = null, int $code = 0)
    {
        parent::__construct($message, $code, $previous);
        $this->additionalData = $additionalData;
    }

    public function getAdditionalData()
    {
        return $this->additionalData;
    }
}
