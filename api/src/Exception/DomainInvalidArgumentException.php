<?php

declare(strict_types=1);

namespace Crawl24\App\Exception;

use InvalidArgumentException;
use Throwable;

final class DomainInvalidArgumentException extends InvalidArgumentException
{
    /** @var mixed */
    private $invalidArgument;

    public function __construct(string $message = '', $invalidArgument, Throwable $previous = null, int $code = 0)
    {
        parent::__construct($message, $code, $previous);
        $this->invalidArgument = $invalidArgument;
    }

    public function getInvalidArgument()
    {
        return $this->invalidArgument;
    }
}
