<?php

declare(strict_types=1);

namespace Crawl24\App\DomCrawler;

use Crawl24\App\Exception\DomainException;
use Crawl24\App\Exception\DomainInvalidArgumentException;
use Crawl24\App\ValueObject\Heading;
use Crawl24\App\ValueObject\HTMLVersion;
use Crawl24\App\ValueObject\Link;
use Fig\Http\Message\RequestMethodInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Promise\PromiseInterface;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheException;
use Psr\SimpleCache\CacheInterface;
use Symfony\Component\DomCrawler\Crawler;
use Throwable;
use function GuzzleHttp\Promise\promise_for;

final class DefaultCrawler implements DomCrawler
{
    private const CACHE_TTL = 100;
    /**
     * @var Crawler
     */
    public $crawler;
    /**
     * @var ClientInterface
     */
    private $client;
    /**
     * @var UriInterface
     */
    private $uri;
    /**
     * @var HTMLVersion
     */
    private $HTMLVersion;
    /**
     * @var CacheInterface
     */
    private $cache;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        Crawler $crawler,
        ClientInterface $client,
        CacheInterface $cache,
        LoggerInterface $logger,
        UriInterface $uri,
        string $htmlContent
    ) {
        try {
            $this->HTMLVersion = HTMLVersion::fromDoctype($htmlContent);
        } catch (DomainInvalidArgumentException $e) {
            throw new DomainException(DomCrawler::WEB_PAGE_IS_NOT_HTML_ERROR_MESSAGE, $htmlContent, $e);
        }
        $crawler->addContent($htmlContent);

        $this->uri = $uri;
        $this->cache = $cache;
        $this->client = $client;
        $this->logger = $logger;
        $this->crawler = $crawler;
    }

    public function getDoctype(): HTMLVersion
    {
        return $this->HTMLVersion;
    }

    public function getTitle(): ?string
    {
        try {
            return $this->crawler->filter('title')->text();
        } catch (InvalidArgumentException $e) {
            return null;
        }
    }

    // May be a set of strategies injected in the constructor
    // but this is a test, and i don't have so much time for this :D
    public function hasFormLogin(): bool
    {
        return $this->crawler->filter('input[type="password"]')->count() > 0;
    }

    /**
     * @return Heading[]
     */
    public function getHeadingCollection(): array
    {
        $headingCollection = [];
        foreach ([1, 2, 3, 4, 5, 6] as $index) {
            $heading = $this->crawler->filter("h$index")->each(function () use ($index) {
                return new Heading($index);
            });
            $headingCollection = \array_merge($headingCollection, $heading);
        }

        return $headingCollection;
    }

    /**
     * @return Link[]
     */
    public function getLinkCollection(): array
    {
        $client = $this->client;

        /** @var PromiseInterface[] $promises */
        $promises = $this->crawler->filter('a')->each(function (Crawler $anchorTag) use ($client) {
            $linkText = $anchorTag->attr('href') ?? ''; // if null return empty and skip later
            $linkText = 0 === \mb_strpos($linkText, '/') ? "{$this->uri->getScheme()}://{$this->uri->getHost()}$linkText" : $linkText;
            $isInternal = $this->uri->getHost() === (new \GuzzleHttp\Psr7\Uri($linkText))->getHost();

            if (false === \filter_var($linkText, FILTER_VALIDATE_URL)) {
                return null;
            }

            try {
                $body = $this->cache->get(\md5((string) $linkText));
                if (null !== $body) {
                    return promise_for(Link::fromString($linkText, true));
                }
            } catch (CacheException | Throwable $e) {
                $this->logger->alert("Impossible retrieve from cache: $linkText", ['exception' => $e]);
            }

            try {
                return $client->requestAsync(RequestMethodInterface::METHOD_GET, $linkText)
                    ->then(function (ResponseInterface $response) use ($linkText, $isInternal) {
                        try {
                            $this->cache->set(\md5((string) $linkText), (string) $response->getBody(), self::CACHE_TTL);
                        } catch (CacheException | Throwable $e) {
                            $this->logger->alert("Impossible save to cache html body for: $linkText", ['exception' => $e]);
                        }

                        return Link::fromString($linkText, true, $isInternal);
                    }, function () use ($linkText, $isInternal) {
                        return Link::fromString($linkText, false, $isInternal);
                    });
            } catch (GuzzleException | DomainInvalidArgumentException $e) {
                return null;
            }
        });

        $links = [];
        $promises = \array_filter($promises);

        foreach ($promises as $promise) {
            $links[] = $promise->wait();
        }

        return \array_filter($links);
    }
}
