<?php

declare(strict_types=1);

namespace Crawl24\App\DomCrawler;

use Crawl24\App\ValueObject\Heading;
use Crawl24\App\ValueObject\HTMLVersion;
use Crawl24\App\ValueObject\Link;

interface DomCrawler
{
    public const WEB_PAGE_IS_NOT_HTML_ERROR_MESSAGE = 'Impossible analyze web page. It is not HTML';

    public function getDoctype(): HTMLVersion;

    public function getTitle(): ?string;

    public function hasFormLogin(): bool;

    /**
     * @return Link[]
     */
    public function getLinkCollection(): array;

    /**
     * @return Heading[]
     */
    public function getHeadingCollection(): array;
}
