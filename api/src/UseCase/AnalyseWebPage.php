<?php

declare(strict_types=1);

namespace Crawl24\App\UseCase;

use Crawl24\App\Aggregate\WebPageAnalyses;
use Crawl24\App\Client\RetrieveWebPageBody;
use Crawl24\App\Factory\DomCrawlerFactory;
use Psr\Http\Message\UriInterface;

class AnalyseWebPage
{
    /**
     * @var RetrieveWebPageBody
     */
    private $retrieveWebPageBody;
    /**
     * @var DomCrawlerFactory
     */
    private $domCrawlerFactory;

    public function __construct(RetrieveWebPageBody $retrieveWebPageBody, DomCrawlerFactory $domCrawlerFactory)
    {
        $this->retrieveWebPageBody = $retrieveWebPageBody;
        $this->domCrawlerFactory = $domCrawlerFactory;
    }

    public function __invoke(UriInterface $uri): WebPageAnalyses
    {
        $html = ($this->retrieveWebPageBody)($uri);
        $crawler = $this->domCrawlerFactory->fromHTMLAndUri($html, $uri);

        return new WebPageAnalyses(
            $crawler->getDoctype(),
            $crawler->getTitle(),
            $crawler->getHeadingCollection(),
            $crawler->getLinkCollection(),
            $crawler->hasFormLogin()
        );
    }
}
