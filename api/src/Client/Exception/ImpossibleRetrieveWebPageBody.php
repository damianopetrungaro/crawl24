<?php

declare(strict_types=1);

namespace Crawl24\App\Client\Exception;

use Exception;
use Throwable;

final class ImpossibleRetrieveWebPageBody extends Exception
{
    /** @var mixed */
    private $additionalData;

    public function __construct(string $message = '', $additionalData = null, Throwable $previous = null, int $code = 0)
    {
        parent::__construct($message, $code, $previous);
        $this->additionalData = $additionalData;
    }

    public function getAdditionalData()
    {
        return $this->additionalData;
    }
}
