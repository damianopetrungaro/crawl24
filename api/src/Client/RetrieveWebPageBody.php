<?php

declare(strict_types=1);

namespace Crawl24\App\Client;

use Psr\Http\Message\UriInterface;

interface RetrieveWebPageBody
{
    public const IMPOSSIBLE_RETRIEVE_WEB_PAGE_CONTENT_ERROR_MESSAGE = '';

    public function __invoke(UriInterface $uri): string;
}
