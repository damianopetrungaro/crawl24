<?php

declare(strict_types=1);

namespace Crawl24\App\Client;

use Crawl24\App\Client\Exception\ImpossibleRetrieveWebPageBody;
use Exception;
use Fig\Http\Message\RequestMethodInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\UriInterface;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheException;
use Psr\SimpleCache\CacheInterface;
use Throwable;

final class ClientRetrieveWebPageBody implements RetrieveWebPageBody
{
    private const CACHE_TTL = 100;

    /**
     * @var ClientInterface
     */
    private $client;
    /**
     * @var CacheInterface
     */
    private $cache;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(ClientInterface $client, CacheInterface $cache, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->cache = $cache;
        $this->logger = $logger;
    }

    /**
     * @throws ImpossibleRetrieveWebPageBody
     */
    public function __invoke(UriInterface $uri): string
    {
        $hashedUri = \md5((string) $uri);
        try {
            $body = $this->cache->get($hashedUri);
            if (null !== $body) {
                return $body;
            }
        } catch (CacheException | Throwable $e) {
            $this->logger->alert("Impossible retrieve from cache: $uri", ['exception' => $e]);
        }

        try {
            $body = $this->client->request(RequestMethodInterface::METHOD_GET, $uri)->getBody()->getContents();
        } catch (Exception | GuzzleException $e) {
            throw new ImpossibleRetrieveWebPageBody(
                self::IMPOSSIBLE_RETRIEVE_WEB_PAGE_CONTENT_ERROR_MESSAGE,
                [(string) $uri],
                $e
            );
        }

        try {
            $this->cache->set($hashedUri, $body, self::CACHE_TTL);
        } catch (CacheException | Throwable $e) {
            $this->logger->alert("Impossible save to cache html body for: $uri", ['exception' => $e]);
        }

        return $body;
    }
}
