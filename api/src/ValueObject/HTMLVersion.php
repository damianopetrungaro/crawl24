<?php

declare(strict_types=1);

namespace Crawl24\App\ValueObject;

use Crawl24\App\Exception\DomainInvalidArgumentException;

final class HTMLVersion
{
    public const INVALID_VERSION_ERROR_MESSAGE = 'HTML version MUST be between one of the supported one: https://www.w3.org/QA/2002/04/valid-dtd-list.html';
    /**
     * Will match any doctype like:
     * "HTML 1.00 specification"
     * "HTML 1.00"
     * "XHTML 1.01".
     */
    private const STANDARD_HTML_VERSIONS = '/<!DOCTYPE html(.*?)(?<version>[X]?HTML[\w\s]*[1-4]{1}.[01]{1,2}[\w\s]*[^\/]*)/mis';

    /**
     * @var string
     */
    private $version;

    private function __construct(string $version)
    {
        $this->version = $version;
    }

    public static function fromDoctype(string $version): self
    {
        if (0 === \mb_stripos(\trim($version), '<!doctype html>')) {
            return new self('5');
        }

        $match = [];
        $result = \preg_match(self::STANDARD_HTML_VERSIONS, $version, $match);
        if (0 === $result || !isset($match['version'])) {
            throw new DomainInvalidArgumentException(self::INVALID_VERSION_ERROR_MESSAGE, $version);
        }

        return new self($match['version']);
    }

    public function version(): string
    {
        return $this->version;
    }
}
