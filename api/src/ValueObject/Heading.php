<?php

declare(strict_types=1);

namespace Crawl24\App\ValueObject;

use Crawl24\App\Exception\DomainInvalidArgumentException;

final class Heading
{
    public const INVALID_SECTION_LEVEL_ERROR_MESSAGE = 'Heading section level MUST be between 1 and 6';

    /**
     * @var int
     */
    private $sectionLevel;

    public function __construct(int $sectionLevel)
    {
        if ($sectionLevel <= 0 || $sectionLevel > 6) {
            throw new DomainInvalidArgumentException(self::INVALID_SECTION_LEVEL_ERROR_MESSAGE, $sectionLevel);
        }

        $this->sectionLevel = $sectionLevel;
    }

    public function sectionLevel(): int
    {
        return $this->sectionLevel;
    }
}
