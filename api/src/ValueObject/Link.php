<?php

declare(strict_types=1);

namespace Crawl24\App\ValueObject;

use Crawl24\App\Exception\DomainInvalidArgumentException;
use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\UriInterface;

final class Link
{
    public const INVALID_LINK_ERROR_MESSAGE = 'Link MUST be a valid Uri';

    /**
     * @var UriInterface
     */
    private $uri;
    /**
     * @var bool
     */
    private $isReachable;
    /**
     * @var bool
     */
    private $isInternal;

    private function __construct(UriInterface $uri, bool $isReachable, bool $isInternal)
    {
        $this->uri = $uri;
        $this->isReachable = $isReachable;
        $this->isInternal = $isInternal;
    }

    public static function fromString(string $link, bool $isReachable, bool $isInternal): self
    {
        if (false === \filter_var($link, FILTER_VALIDATE_URL)) {
            throw new DomainInvalidArgumentException(self::INVALID_LINK_ERROR_MESSAGE, [$link, $isReachable]);
        }

        return new self(new Uri($link), $isReachable, $isInternal);
    }

    public function uri(): string
    {
        return $this->uri->__toString();
    }

    public function isReachable(): bool
    {
        return $this->isReachable;
    }

    public function isInternal(): bool
    {
        return $this->isInternal;
    }
}
