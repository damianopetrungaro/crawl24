<?php

declare(strict_types=1);

namespace Crawl24\App\Factory;

use Crawl24\App\DomCrawler\DefaultCrawler;
use Crawl24\App\DomCrawler\DomCrawler;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\UriInterface;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;
use Symfony\Component\DomCrawler\Crawler;

class DomCrawlerFactory
{
    /**
     * @var Crawler
     */
    private $crawler;
    /**
     * @var ClientInterface
     */
    private $client;
    /**
     * @var CacheInterface
     */
    private $cache;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(Crawler $crawler, ClientInterface $client, CacheInterface $cache, LoggerInterface $logger)
    {
        $this->crawler = $crawler;
        $this->client = $client;
        $this->cache = $cache;
        $this->logger = $logger;
    }

    public function fromHTMLAndUri(string $htmlContent, UriInterface $uri): DomCrawler
    {
        return new DefaultCrawler(
            $this->crawler,
            $this->client,
            $this->cache,
            $this->logger,
            $uri,
            $htmlContent
        );
    }
}
