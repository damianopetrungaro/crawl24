<?php

declare(strict_types=1);

namespace Crawl24\App\Controller;

use Crawl24\App\Aggregate\WebPageAnalyses;
use Crawl24\App\UseCase\AnalyseWebPage as AnalyseWebPageUseCase;
use Crawl24\App\ValueObject\Heading;
use Crawl24\App\ValueObject\Link;
use Fig\Http\Message\StatusCodeInterface;
use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Throwable;
use function GuzzleHttp\Psr7\stream_for;

final class AnalyseWebPage
{
    /**
     * @var AnalyseWebPageUseCase
     */
    private $analyseWebPage;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(AnalyseWebPageUseCase $analyseWebPage, LoggerInterface $logger)
    {
        $this->analyseWebPage = $analyseWebPage;
        $this->logger = $logger;
    }

    public function __invoke(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $uri = \json_decode($request->getBody()->getContents(), true)['uri'] ?? null;

        if (null === $uri) {
            return $response
                ->withBody(stream_for('Please send a valid uri to crawl'))
                ->withStatus(StatusCodeInterface::STATUS_BAD_REQUEST);
        }

        if (false === \filter_var($uri, FILTER_VALIDATE_URL)) {
            $this->logger->alert('Invalid uri', ['uri' => $uri]);

            return $response
                ->withBody(stream_for('Please send a valid uri to crawl'))
                ->withStatus(StatusCodeInterface::STATUS_BAD_REQUEST);
        }

        try {
            /** @var WebPageAnalyses $webPageAnalyses */
            $webPageAnalyses = ($this->analyseWebPage)(new Uri($uri));
        } catch (Throwable $e) {
            $this->logger->alert($e->getMessage(), ['exception' => $e]);

            return $response
                ->withBody(stream_for('Sorry, an error occurred!'))
                ->withStatus(StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR);
        }

        $mapped = [
            'version' => $webPageAnalyses->HTMLVersion()->version(),
            'page_title' => $webPageAnalyses->pageTitle(),
            'headings' => \array_map([$this, 'headingToScalar'], $webPageAnalyses->headingsCollection()),
            'links' => \array_map([$this, 'linkToScalar'], $webPageAnalyses->linksCollection()),
            'has_login_form' => $webPageAnalyses->hasLoginForm(),
        ];

        return $response->withAddedHeader('Content-Type', 'application/json')->withBody(stream_for(\json_encode($mapped)));
    }

    private function headingToScalar(Heading $heading): int
    {
        return $heading->sectionLevel();
    }

    private function linkToScalar(Link $link): string
    {
        return \json_encode([
            'uri' => $link->uri(),
            'is_reachable' => $link->isReachable(),
            'is_internal' => $link->isInternal(),
        ]);
    }
}
