<?php

declare(strict_types=1);

namespace Crawl24\App\Aggregate;

use Crawl24\App\Exception\DomainInvalidArgumentException;
use Crawl24\App\ValueObject\Heading;
use Crawl24\App\ValueObject\HTMLVersion;
use Crawl24\App\ValueObject\Link;
use InvalidArgumentException;

final class WebPageAnalyses
{
    /**
     * @var HTMLVersion
     */
    private $HTMLVersion;

    /**
     * @var null|string
     */
    private $pageTitle;

    /**
     * @var array
     */
    private $headingsCollection;

    /**
     * @var array
     */
    private $linksCollection;

    /**
     * @var bool
     */
    private $hasLoginForm;

    /**
     * @param Heading[] $headingsCollection
     * @param Link[]    $linksCollection
     *
     * @throws InvalidArgumentException
     */
    public function __construct(HTMLVersion $HTMLVersion, ?string $pageTitle, array $headingsCollection, array $linksCollection, bool $hasLoginForm)
    {
        \array_walk($headingsCollection, [$this, 'checkInstanceOf'], Heading::class);
        \array_walk($linksCollection, [$this, 'checkInstanceOf'], Link::class);

        $this->HTMLVersion = $HTMLVersion;
        $this->pageTitle = $pageTitle;
        $this->hasLoginForm = $hasLoginForm;
        $this->headingsCollection = $headingsCollection;
        $this->linksCollection = $linksCollection;
    }

    public function HTMLVersion(): HTMLVersion
    {
        return $this->HTMLVersion;
    }

    public function pageTitle(): ?string
    {
        return $this->pageTitle;
    }

    public function hasLoginForm(): bool
    {
        return $this->hasLoginForm;
    }

    /**
     * @return Heading[]
     */
    public function headingsCollection(): array
    {
        return $this->headingsCollection;
    }

    /**
     * @return Link[]
     */
    public function linksCollection(): array
    {
        return $this->linksCollection;
    }

    /**
     * @throws InvalidArgumentException
     */
    private function checkInstanceOf($elem, int $index, string $requiredClassName): void
    {
        if (!$elem instanceof $requiredClassName) {
            $subject = \is_object($elem) ? \get_class($elem) : $elem;

            throw new DomainInvalidArgumentException(
                "$subject must be a $requiredClassName",
                [$index => $elem]
            );
        }
    }
}
