<?php

declare(strict_types=1);

$entries = [];

$entries['settings'] = [
    'cacheDirectory' => \getenv('CACHE_DIRECTORY'),
    'logsDirectory' => \getenv('LOGS_DIRECTORY'),
    'appUrl' => \getenv('BASE_APP_URL'),
];

$entries['mainErrorHandler'] = function (Psr\Container\ContainerInterface $container) {
    return function (Psr\Http\Message\RequestInterface $request, Psr\Http\Message\ResponseInterface $response, Throwable $e) use ($container) {
        $logger = $container->get(\Psr\Log\LoggerInterface::class);
        $logger->error($e->getMessage(), ['stack' => $e]);

        return $response->withStatus(\Fig\Http\Message\StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR);
    };
};

$entries['phpErrorHandler'] = function (Psr\Container\ContainerInterface $container) {
    return $container->get('mainErrorHandler');
};

$entries['errorHandler'] = function (Psr\Container\ContainerInterface $container) {
    return $container->get('mainErrorHandler');
};

$entries['notFoundHandler'] = function () {
    return function (Psr\Http\Message\RequestInterface $request, Psr\Http\Message\ResponseInterface $response) {
        return $response->withStatus(\Fig\Http\Message\StatusCodeInterface::STATUS_NOT_FOUND);
    };
};

$entries[\Psr\Log\LoggerInterface::class] = function (Psr\Container\ContainerInterface $container) {
    $file = "{$container->get('settings')['logsDirectory']}app.log";

    return new \Monolog\Logger('app', [new \Monolog\Handler\RotatingFileHandler($file)]);
};

$entries[\GuzzleHttp\ClientInterface::class] = function (Psr\Container\ContainerInterface $container) {
    return new\GuzzleHttp\Client();
};

$entries[\Psr\SimpleCache\CacheInterface::class] = function (Psr\Container\ContainerInterface $container) {
    return new Symfony\Component\Cache\Simple\FilesystemCache('', 0, $container->get('settings')['logsDirectory']);
};

$entries[Crawl24\App\Client\RetrieveWebPageBody::class] = function (Psr\Container\ContainerInterface $container) {
    return new Crawl24\App\Client\ClientRetrieveWebPageBody(
        $container->get(\GuzzleHttp\ClientInterface::class),
        $container->get(\Psr\SimpleCache\CacheInterface::class),
        $container->get(\Psr\Log\LoggerInterface::class)
    );
};

$entries[Crawl24\App\Factory\DomCrawlerFactory::class] = function (Psr\Container\ContainerInterface $container) {
    return new Crawl24\App\Factory\DomCrawlerFactory(
        new Symfony\Component\DomCrawler\Crawler(),
        $container->get(\GuzzleHttp\ClientInterface::class),
        $container->get(\Psr\SimpleCache\CacheInterface::class),
        $container->get(\Psr\Log\LoggerInterface::class)
    );
};

$entries[Crawl24\App\UseCase\AnalyseWebPage::class] = function (Psr\Container\ContainerInterface $container) {
    return new Crawl24\App\UseCase\AnalyseWebPage(
        $container->get(Crawl24\App\Client\RetrieveWebPageBody::class),
        $container->get(Crawl24\App\Factory\DomCrawlerFactory::class)
    );
};

$entries[Crawl24\App\Controller\AnalyseWebPage::class] = function (Psr\Container\ContainerInterface $container) {
    return new Crawl24\App\Controller\AnalyseWebPage(
        $container->get(Crawl24\App\UseCase\AnalyseWebPage::class),
        $container->get(\Psr\Log\LoggerInterface::class)
    );
};

return new \Slim\Container($entries);
