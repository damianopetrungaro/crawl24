<?php

declare(strict_types=1);

namespace Crawl24\App\DomCrawler;

use Crawl24\App\Exception\DomainException;
use Crawl24\App\TestCase;
use Crawl24\App\ValueObject\Heading;
use Crawl24\App\ValueObject\Link;
use Faker\Factory;
use Faker\Generator;
use Fig\Http\Message\RequestMethodInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Uri;
use Prophecy\Argument;
use Psr\Http\Message\UriInterface;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;
use Symfony\Component\DomCrawler\Crawler;
use function GuzzleHttp\Promise\promise_for;

final class DefaultCrawlerTest extends TestCase
{
    public const OK_PROMISE_RESOLUTION = 'OK_PROMISE_RESOLUTION';
    public const FAIL_PROMISE_RESOLUTION = 'FAIL_PROMISE_RESOLUTION';

    /**
     * @dataProvider invalid_html_page_data_provider
     */
    public function test_content(string $content): void
    {
        $this->expectException(DomainException::class);
        $client = $this->prophesize(ClientInterface::class)->reveal();
        $uri = $this->prophesize(UriInterface::class)->reveal();
        $cache = $this->prophesize(CacheInterface::class)->reveal();
        $logger = $this->prophesize(LoggerInterface::class)->reveal();
        $symfonyCrawler = $this->prophesize(Crawler::class)->reveal();

        new DefaultCrawler($symfonyCrawler, $client, $cache, $logger, $uri, $content);
    }

    /**
     * @dataProvider valid_html_page_data_provider
     */
    public function test_html_doctype_is_properly_choose(string $content, string $expectedDoctype): void
    {
        $client = $this->prophesize(ClientInterface::class)->reveal();
        $uri = $this->prophesize(UriInterface::class)->reveal();
        $cache = $this->prophesize(CacheInterface::class)->reveal();
        $logger = $this->prophesize(LoggerInterface::class)->reveal();
        $symfonyCrawler = $this->prophesize(Crawler::class)->reveal();

        $crawler = new DefaultCrawler($symfonyCrawler, $client, $cache, $logger, $uri, $content);
        $this->assertSame($expectedDoctype, $crawler->getDoctype()->version());
    }

    /**
     * @dataProvider HTML_title_data_provider
     */
    public function test_title_is_properly_created(string $htmlContent, ?string $expectedTitle): void
    {
        $client = $this->prophesize(ClientInterface::class)->reveal();
        $uri = $this->prophesize(UriInterface::class)->reveal();
        $cache = $this->prophesize(CacheInterface::class)->reveal();
        $logger = $this->prophesize(LoggerInterface::class)->reveal();
        $symfonyCrawler = new Crawler();

        $crawler = new DefaultCrawler($symfonyCrawler, $client, $cache, $logger, $uri, $htmlContent);
        $this->assertSame($expectedTitle, $crawler->getTitle());
    }

    /**
     * @dataProvider HTML_form_data_provider
     */
    public function test_login_form_exists(string $htmlContent, bool $hasLoginForm): void
    {
        $client = $this->prophesize(ClientInterface::class)->reveal();
        $uri = $this->prophesize(UriInterface::class)->reveal();
        $cache = $this->prophesize(CacheInterface::class)->reveal();
        $logger = $this->prophesize(LoggerInterface::class)->reveal();
        $symfonyCrawler = new Crawler();

        $crawler = new DefaultCrawler($symfonyCrawler, $client, $cache, $logger, $uri, $htmlContent);
        $this->assertSame($hasLoginForm, $crawler->hasFormLogin());
    }

    /**
     * @dataProvider HTML_headings_data_provider
     */
    public function test_heading_collection(string $htmlContent, array $expectedHeadingCollection): void
    {
        $client = $this->prophesize(ClientInterface::class)->reveal();
        $uri = $this->prophesize(UriInterface::class)->reveal();
        $cache = $this->prophesize(CacheInterface::class)->reveal();
        $logger = $this->prophesize(LoggerInterface::class)->reveal();
        $symfonyCrawler = new Crawler();

        $crawler = new DefaultCrawler($symfonyCrawler, $client, $cache, $logger, $uri, $htmlContent);
        $this->assertEquals($expectedHeadingCollection, $crawler->getHeadingCollection());
    }

    /**
     * @dataProvider HTML_links_data_provider
     */
    public function test_link_collection(string $htmlContent, UriInterface $baseUri, array $listOfUriToCallAndResult, array $expectedLinkCollection): void
    {
        $client = $this->prophesize(ClientInterface::class);
        $cache = $this->prophesize(CacheInterface::class);
        foreach ($listOfUriToCallAndResult as $uri) {
            $isInternal = $baseUri->getHost() === (new Uri($uri))->getHost();
            $promise = $this->prophesize(PromiseInterface::class);
            $client->requestAsync(RequestMethodInterface::METHOD_GET, $uri)->shouldBeCalledTimes(1)->willReturn($promise);
            $promise->then(Argument::any(), Argument::any())->willReturn(promise_for(Link::fromString($uri, true, $isInternal)));
        }
        $client = $client->reveal();
        $cache = $cache->reveal();
        $logger = $this->prophesize(LoggerInterface::class)->reveal();
        $symfonyCrawler = new Crawler();

        $crawler = new DefaultCrawler($symfonyCrawler, $client, $cache, $logger, $baseUri, $htmlContent);
        $this->assertEquals($expectedLinkCollection, $crawler->getLinkCollection());
    }

    /**
     * @dataProvider HTML_links_data_provider
     */
    public function test_invalid_link_collection(): void
    {
        $uri = 'http://google.com/';
        $client = $this->prophesize(ClientInterface::class);
        $client->requestAsync(RequestMethodInterface::METHOD_GET, $uri)->willThrow(ClientException::class);
        $client = $client->reveal();
        $cache = $this->prophesize(CacheInterface::class)->reveal();
        $logger = $this->prophesize(LoggerInterface::class)->reveal();
        $symfonyCrawler = new Crawler();

        $crawler = new DefaultCrawler($symfonyCrawler, $client, $cache, $logger, new Uri($uri), "<!doctype html><a href='$uri'>");
        $this->assertEquals([], $crawler->getLinkCollection());
    }

    public function invalid_html_page_data_provider(): array
    {
        /** @var Generator $faker */
        $faker = $this->factoryFaker->instance(Factory::class);

        return [
            [$faker->randomHtml(), false],
            [$faker->sentence(), false],
            [$faker->text(), false],
        ];
    }

    public function valid_html_page_data_provider(): array
    {
        /** @var Generator $faker */
        $faker = $this->factoryFaker->instance(Factory::class);

        return [
            ['<!doctype html>'.$faker->randomHtml(), '5'],
            [
                '<!DOCTYPE html PUBLIC'.PHP_EOL.'"-//W3C//DTD XHTML Basic 1.1//EN"'.PHP_EOL.'"http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">'.$faker->randomHtml(),
                'XHTML Basic 1.1',
            ],
            [
                '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"'.PHP_EOL.'"http://www.w3.org/TR/html4/strict.dtd">'.$faker->randomHtml(),
                'HTML 4.01',
            ],
        ];
    }

    public function HTML_title_data_provider(): array
    {
        return [
            ['<!doctype html><title>This is an example title</title>', 'This is an example title'],
            ['<!doctype html><html></html>', null],
            ['<!doctype html><title>0 1 2 </title>', '0 1 2 '],
        ];
    }

    public function HTML_form_data_provider(): array
    {
        return [
            ['<!doctype html><title>...</title><form method="post"><input type="password"></form>', true],
            ['<!doctype html><title>...</title><form method="post"><input type="text"><input type="password"></form>', true],
            ['<!doctype html><title>...</title><form method="post"><input type="text"><select></select>><input type="text"> </form>', false],
        ];
    }

    public function HTML_headings_data_provider(): array
    {
        return [
            ['<!doctype html><h0></h0><h1></h1><h1></h1><h1></h1><h2></h2><h2></h2><h6></h6><h10></h10>', [
                new Heading(1),
                new Heading(1),
                new Heading(1),
                new Heading(2),
                new Heading(2),
                new Heading(6),
            ]],
            ['<!doctype html><title>...</title>', []],
        ];
    }

    public function HTML_links_data_provider(): array
    {
        return [
            [
                "<!doctype html><a href='https://www.google.com/one'><a href='http://another-one.it/two'><a href='/three'>",
                new Uri('https://www.google.com/'),
                [
                    'https://www.google.com/one',
                    'http://another-one.it/two',
                    'https://www.google.com/three',
                ],
                [
                    Link::fromString('https://www.google.com/one', true, true),
                    Link::fromString('http://another-one.it/two', true, false),
                    Link::fromString('https://www.google.com/three', true, true),
                ],
            ],
        ];
    }
}
