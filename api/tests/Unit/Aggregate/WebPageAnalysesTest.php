<?php

declare(strict_types=1);

namespace Crawl24\App\Aggregate;

use Crawl24\App\Exception\DomainInvalidArgumentException;
use Crawl24\App\TestCase;
use Crawl24\App\ValueObject\Heading;
use Crawl24\App\ValueObject\HTMLVersion;
use Crawl24\App\ValueObject\Link;
use Faker\Factory;
use Faker\Generator;

final class WebPageAnalysesTest extends TestCase
{
    /**
     * @dataProvider valid_web_page_analyses_data_provider
     */
    public function test_web_page_analyses_is_properly_created(
        HTMLVersion $html,
        ?string $pageTitle,
        array $headingCollection,
        array $linkCollection,
        bool $hasLoginForm
    ): void {
        $webPageAnalyses = new WebPageAnalyses($html, $pageTitle, $headingCollection, $linkCollection, $hasLoginForm);
        $this->assertSame($html, $webPageAnalyses->HTMLVersion());
        $this->assertSame($pageTitle, $webPageAnalyses->pageTitle());
        $this->assertSame($headingCollection, $webPageAnalyses->headingsCollection());
        $this->assertSame($linkCollection, $webPageAnalyses->linksCollection());
        $this->assertSame($hasLoginForm, $webPageAnalyses->hasLoginForm());
    }

    /**
     * @dataProvider invalid_web_page_analyses_data_provider
     */
    public function test_web_page_analyses_thrown_domain_invalid_argument_exception(
        HTMLVersion $html,
        string $pageTitle,
        array $headingCollection,
        array $linkCollection,
        bool $hasLoginForm,
        string $expectedErrorMessage
    ): void {
        $this->expectException(DomainInvalidArgumentException::class);
        $this->expectExceptionMessage($expectedErrorMessage);
        new WebPageAnalyses($html, $pageTitle, $headingCollection, $linkCollection, $hasLoginForm);
    }

    public function valid_web_page_analyses_data_provider(): array
    {
        /** @var Generator $faker */
        $faker = $this->factoryFaker->instance(Factory::class);

        return [
            [
                $this->factoryFaker->instance(HTMLVersion::class),
                $faker->sentence(),
                \array_fill(0, 4, $this->factoryFaker->instance(Heading::class)),
                \array_fill(0, 4, $this->factoryFaker->instance(Link::class)),
                $faker->boolean(),
            ], [
                $this->factoryFaker->instance(HTMLVersion::class),
                null,
                [],
                [],
                $faker->boolean(),
            ],
        ];
    }

    public function invalid_web_page_analyses_data_provider(): array
    {
        /** @var Generator $faker */
        $faker = $this->factoryFaker->instance(Factory::class);

        return [
            [
                $this->factoryFaker->instance(HTMLVersion::class),
                $faker->sentence(),
                \array_fill(0, 4, $this->factoryFaker->instance(HTMLVersion::class)),
                \array_fill(0, 4, $this->factoryFaker->instance(Link::class)),
                $faker->boolean(),
                HTMLVersion::class.' must be a '.Heading::class,
            ], [
                $this->factoryFaker->instance(HTMLVersion::class),
                $faker->sentence(),
                [],
                \array_fill(0, 4, $this->factoryFaker->instance(Heading::class)),
                $faker->boolean(),
                Heading::class.' must be a '.Link::class,
            ], [
                $this->factoryFaker->instance(HTMLVersion::class),
                $faker->sentence(),
                [10],
                \array_fill(0, 4, $this->factoryFaker->instance(Heading::class)),
                $faker->boolean(),
                '10 must be a '.Heading::class,
            ],
        ];
    }
}
