<?php

declare(strict_types=1);

namespace Crawl24\App\UseCase;

use Crawl24\App\Aggregate\WebPageAnalyses;
use Crawl24\App\Client\RetrieveWebPageBody;
use Crawl24\App\DomCrawler\DomCrawler;
use Crawl24\App\Factory\DomCrawlerFactory;
use Crawl24\App\TestCase;
use Crawl24\App\ValueObject\Heading;
use Crawl24\App\ValueObject\HTMLVersion;
use Crawl24\App\ValueObject\Link;
use Faker\Factory;
use Faker\Generator;
use Psr\Http\Message\UriInterface;
use Slim\Http\Uri;

final class AnalyseWebPageTest extends TestCase
{
    /**
     * @dataProvider analyse_web_page_data_provider
     */
    public function test_analyse_web_page(UriInterface $uri, string $retrievedBody, DomCrawler $domCrawler): void
    {
        $retrieveWebPageBody = $this->prophesize(RetrieveWebPageBody::class);
        $retrieveWebPageBody->__invoke($uri)->willReturn($retrievedBody);
        $retrieveWebPageBody = $retrieveWebPageBody->reveal();
        $domCrawlerFactory = $this->prophesize(DomCrawlerFactory::class);
        $domCrawlerFactory->fromHTMLAndUri($retrievedBody, $uri)->willReturn($domCrawler);
        $domCrawlerFactory = $domCrawlerFactory->reveal();

        $analyseWebPage = new AnalyseWebPage($retrieveWebPageBody, $domCrawlerFactory);
        $this->assertInstanceOf(WebPageAnalyses::class, $analyseWebPage($uri));
    }

    public function analyse_web_page_data_provider(): array
    {
        /** @var Generator $faker */
        $faker = $this->factoryFaker->instance(Factory::class);
        $crawler = $this->prophesize(DomCrawler::class);
        $crawler->getTitle()->willReturn($faker->sentence());
        $crawler->getDoctype()->willReturn($this->factoryFaker->instance(HTMLVersion::class));
        $crawler->getLinkCollection()->willReturn(\array_fill(0, 4, $this->factoryFaker->instance(Link::class)));
        $crawler->getHeadingCollection()->willReturn(\array_fill(0, 4, $this->factoryFaker->instance(Heading::class)));
        $crawler->hasFormLogin()->willReturn($faker->boolean());
        $crawler = $crawler->reveal();

        return \array_fill(0, 5, [Uri::createFromString($faker->url), $faker->sentence(), $crawler]);
    }
}
