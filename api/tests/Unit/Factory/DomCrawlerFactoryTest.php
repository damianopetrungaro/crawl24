<?php

declare(strict_types=1);

namespace Crawl24\App\Factory;

use Crawl24\App\DomCrawler\DomCrawler;
use Crawl24\App\TestCase;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Uri;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;
use Symfony\Component\DomCrawler\Crawler;

final class DomCrawlerFactoryTest extends TestCase
{
    public function test_factory_create_web_page_analses()
    {
        $crawler = $this->prophesize(Crawler::class)->reveal();
        $client = $this->prophesize(ClientInterface::class)->reveal();
        $cache = $this->prophesize(CacheInterface::class)->reveal();
        $logger = $this->prophesize(LoggerInterface::class)->reveal();
        $crawlerFactory = new DomCrawlerFactory($crawler, $client, $cache, $logger);
        $domCrawler = $crawlerFactory->fromHTMLAndUri('<!doctype html>', new Uri('http://www.google.com'));
        $this->assertInstanceOf(DomCrawler::class, $domCrawler);
    }
}
