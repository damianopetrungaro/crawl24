<?php

declare(strict_types=1);

namespace Crawl24\App;

use League\FactoryMuffin\FactoryMuffin;

abstract class TestCase extends \PHPUnit\Framework\TestCase
{
    /**
     * @var FactoryMuffin
     */
    protected $factoryFaker;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        $fm = new FactoryMuffin();
        $fm->loadFactories(__DIR__.'/../factories');
        $this->factoryFaker = $fm;
        parent::__construct($name, $data, $dataName);
    }
}
