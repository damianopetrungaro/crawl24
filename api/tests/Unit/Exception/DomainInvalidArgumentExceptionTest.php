<?php

declare(strict_types=1);

namespace Crawl24\App\Exception;

use Exception;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Throwable;

final class DomainInvalidArgumentExceptionTest extends TestCase
{
    /**
     * @dataProvider valid_argument_exception_data_provider
     */
    public function test_domain_invalid_argument_exception(
        string $message,
        $invalidArgument,
        ?Throwable $previous,
        int $code
    ): void {
        $exception = new DomainInvalidArgumentException($message, $invalidArgument, $previous, $code);
        $this->assertSame($message, $exception->getMessage());
        $this->assertSame($invalidArgument, $exception->getInvalidArgument());
        $this->assertSame($previous, $exception->getPrevious());
        $this->assertSame($code, $exception->getCode());
    }

    public function valid_argument_exception_data_provider(): array
    {
        return [
            [
                'Message',
                'invalid argument',
                new Exception(),
                0,
            ],
            [
                '',
                null,
                null,
                0,
            ],
            [
                'this is a message!',
                123.2,
                new InvalidArgumentException('invalid'),
                100,
            ],
        ];
    }
}
