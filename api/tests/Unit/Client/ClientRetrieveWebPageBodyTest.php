<?php

declare(strict_types=1);

namespace Crawl24\App\Client;

use Crawl24\App\Client\Exception\ImpossibleRetrieveWebPageBody;
use Crawl24\App\TestCase;
use Exception;
use Faker\Factory;
use Faker\Generator;
use Fig\Http\Message\RequestMethodInterface;
use GuzzleHttp\ClientInterface;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;
use Slim\Http\Uri;
use function GuzzleHttp\Psr7\stream_for;

final class ClientRetrieveWebPageBodyTest extends TestCase
{
    /**
     * @dataProvider cached_content_data_provider
     */
    public function test_client_use_cache_before_retrieving_html(UriInterface $uri, string $webPageBody): void
    {
        $logger = $this->prophesize(LoggerInterface::class)->reveal();
        $client = $this->prophesize(ClientInterface::class)->reveal();
        $cache = $this->prophesize(CacheInterface::class);
        $cache->get(\md5((string) $uri))->willReturn($webPageBody);
        $cache = $cache->reveal();

        $retrieveWebPageBody = new ClientRetrieveWebPageBody($client, $cache, $logger);
        $this->assertSame($webPageBody, $retrieveWebPageBody($uri));
    }

    /**
     * @dataProvider cached_content_data_provider
     */
    public function test_client_use_client_when_cache_is_empty(UriInterface $uri, string $webPageBody): void
    {
        $hashedUri = \md5((string) $uri);

        $logger = $this->prophesize(LoggerInterface::class)->reveal();
        $cache = $this->prophesize(CacheInterface::class);
        $cache->get($hashedUri)->willReturn(null);
        $cache->set($hashedUri, $webPageBody, Argument::type('integer'))->shouldBeCalledTimes(1);
        $cache = $cache->reveal();
        $response = $this->prophesize(ResponseInterface::class);
        $response->getBody()->willReturn(stream_for($webPageBody));
        $response->reveal();
        $client = $this->prophesize(ClientInterface::class);
        $client->request(RequestMethodInterface::METHOD_GET, $uri)->willReturn($response);
        $client = $client->reveal();

        $retrieveWebPageBody = new ClientRetrieveWebPageBody($client, $cache, $logger);
        $this->assertSame($webPageBody, $retrieveWebPageBody($uri));
    }

    /**
     * @dataProvider cached_content_data_provider
     */
    public function test_client_is_used_logging_alert_when_cache_fails(UriInterface $uri, string $webPageBody): void
    {
        $hashedUri = \md5((string) $uri);

        $logger = $this->prophesize(LoggerInterface::class);
        $logger->alert(Argument::containingString((string) $uri), Argument::any())->shouldBeCalledTimes(2);
        $logger = $logger->reveal();

        $cache = $this->prophesize(CacheInterface::class);
        $cache->get($hashedUri)->willThrow(Exception::class);
        $cache->set($hashedUri, $webPageBody, Argument::type('integer'))->willThrow(Exception::class);
        $cache = $cache->reveal();

        $response = $this->prophesize(ResponseInterface::class);
        $response->getBody()->willReturn(stream_for($webPageBody));
        $response->reveal();

        $client = $this->prophesize(ClientInterface::class);
        $client->request(RequestMethodInterface::METHOD_GET, $uri)->willReturn($response);
        $client = $client->reveal();

        $retrieveWebPageBody = new ClientRetrieveWebPageBody($client, $cache, $logger);
        $retrieveWebPageBody($uri);
    }

    /**
     * @dataProvider cached_content_data_provider
     */
    public function test_client_throws_impossible_retrieve_web_page_body_when_client_fails(UriInterface $uri, string $_): void
    {
        $this->expectException(ImpossibleRetrieveWebPageBody::class);
        $client = $this->prophesize(ClientInterface::class);
        $client->request(RequestMethodInterface::METHOD_GET, $uri)->willThrow(Exception::class);
        $client = $client->reveal();
        $logger = $this->prophesize(LoggerInterface::class)->reveal();
        $cache = $this->prophesize(CacheInterface::class)->reveal();

        $retrieveWebPageBody = new ClientRetrieveWebPageBody($client, $cache, $logger);
        $retrieveWebPageBody($uri);
    }

    public function cached_content_data_provider(): array
    {
        /** @var Generator $faker */
        $faker = $this->factoryFaker->instance(Factory::class);

        return \array_fill(0, 5, [Uri::createFromString($faker->url), $faker->randomHtml()]);
    }
}
