<?php

declare(strict_types=1);

namespace Crawl24\App\Client\Exception;

use Exception;
use PHPUnit\Framework\TestCase;
use Throwable;

final class ImpossibleRetrieveWebPageBodyTest extends TestCase
{
    /**
     * @dataProvider valid_argument_exception_data_provider
     */
    public function test_impossible_retrieve_web_page_body_exception(
        string $message,
        $invalidArgument,
        ?Throwable $previous,
        int $code
    ): void {
        $exception = new ImpossibleRetrieveWebPageBody($message, $invalidArgument, $previous, $code);
        $this->assertSame($message, $exception->getMessage());
        $this->assertSame($invalidArgument, $exception->getAdditionalData());
        $this->assertSame($previous, $exception->getPrevious());
        $this->assertSame($code, $exception->getCode());
    }

    public function valid_argument_exception_data_provider(): array
    {
        return [
            [
                'Message',
                'invalid argument',
                new Exception(),
                0,
            ],
            [
                '',
                null,
                null,
                0,
            ],
            [
                'this is a message!',
                123.2,
                new Exception('invalid'),
                100,
            ],
        ];
    }
}
