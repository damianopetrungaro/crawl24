<?php

declare(strict_types=1);

namespace Crawl24\App\ValueObject;

use Crawl24\App\Exception\DomainInvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class LinkTest extends TestCase
{
    /**
     * @dataProvider valid_section_level_data_provider
     */
    public function test_link_is_properly_created(string $uri, bool $isReachable, bool $isInternal): void
    {
        $link = Link::fromString($uri, $isReachable, $isInternal);
        $this->assertSame($isInternal, $link->isInternal());
        $this->assertSame($isReachable, $link->isReachable());
        $this->assertSame($uri, $link->uri());
    }

    /**
     * @dataProvider invalid_section_level_data_provider
     */
    public function test_link_throws_domain_exception(string $uri, bool $isReachable, bool $isInternal): void
    {
        $this->expectExceptionMessage(Link::INVALID_LINK_ERROR_MESSAGE);
        $this->expectException(DomainInvalidArgumentException::class);
        Link::fromString($uri, $isReachable, $isInternal);
    }

    public function valid_section_level_data_provider(): array
    {
        return [
            ['https://www.google.com', true, false],
            ['http://www.google.com', false, true],
            ['ftp://google.com?query=string', true, true],
            ['http://google.com/sub/page', false, false],
        ];
    }

    public function invalid_section_level_data_provider(): array
    {
        return [
            ['those', true, false],
            ['are all', false, true],
            ['invalid URLs', true, true],
            ['google_com/.sub/page', false, false],
        ];
    }
}
