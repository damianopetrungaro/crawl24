<?php

declare(strict_types=1);

namespace Crawl24\App\ValueObject;

use Crawl24\App\Exception\DomainInvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class HeadingTest extends TestCase
{
    /**
     * @dataProvider valid_section_level_data_provider
     */
    public function test_heading_is_properly_created(int $sectionLevel): void
    {
        $heading = new Heading($sectionLevel);
        $this->assertSame($sectionLevel, $heading->sectionLevel());
    }

    /**
     * @dataProvider invalid_section_level_data_provider
     */
    public function test_heading_throws_domain_exception(int $invalidSectionLevel): void
    {
        $this->expectExceptionMessage(Heading::INVALID_SECTION_LEVEL_ERROR_MESSAGE);
        $this->expectException(DomainInvalidArgumentException::class);
        new Heading($invalidSectionLevel);
    }

    public function valid_section_level_data_provider(): array
    {
        return [
            [1], [2], [3], [4], [5], [6],
        ];
    }

    public function invalid_section_level_data_provider(): array
    {
        return [
            [0], [-1], [12], [8],
        ];
    }
}
