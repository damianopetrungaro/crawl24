<?php

declare(strict_types=1);

namespace Crawl24\App\ValueObject;

use Crawl24\App\Exception\DomainInvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class HTMLVersionTest extends TestCase
{
    /**
     * @dataProvider valid_HTML_version_data_provider
     */
    public function test_html_version_is_properly_created(string $doctype, string $expectedVersion): void
    {
        $htmlVersion = HTMLVersion::fromDoctype($doctype);
        $this->assertSame($expectedVersion, $htmlVersion->version());
    }

    /**
     * @dataProvider invalid_HTML_version_data_provider
     */
    public function test_html_version_throws_domain_exception(string $doctype): void
    {
        $this->expectExceptionMessage(HTMLVersion::INVALID_VERSION_ERROR_MESSAGE);
        $this->expectException(DomainInvalidArgumentException::class);
        HTMLVersion::fromDoctype($doctype);
    }

    public function valid_HTML_version_data_provider(): array
    {
        return [
            [
                '<!DOCTYPE html PUBLIC'.PHP_EOL.'"-//W3C//DTD XHTML Basic 1.1//EN"'.PHP_EOL.'"http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">',
                'XHTML Basic 1.1',
            ], [
                '<!doctype html>',
                '5',
            ], [
                '<!DOCTYPE html>',
                '5',
            ], [
                '<!doctype HTML>',
                '5',
            ], [
                '<!DOCTYPE html PUBLIC'.PHP_EOL.'"-//W3C//DTD XHTML 1.0 Transitional//EN"'.PHP_EOL.'"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">',
                'XHTML 1.0 Transitional',
            ], [
                '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"'.PHP_EOL.'"http://www.w3.org/TR/html4/strict.dtd">',
                'HTML 4.01',
            ],
        ];
    }

    public function invalid_HTML_version_data_provider(): array
    {
        return [
            ['a string'],
            ['<doctype>'],
            ['<h1><h1>'],
            ['!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"'.PHP_EOL.'"http://www.w3.org/TR/html4/strict.dtd"'],
        ];
    }
}
