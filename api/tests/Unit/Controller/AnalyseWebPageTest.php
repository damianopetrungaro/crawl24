<?php

declare(strict_types=1);

namespace Crawl24\App\Controller;

use Crawl24\App\Aggregate\WebPageAnalyses;
use Crawl24\App\TestCase;
use Crawl24\App\UseCase\AnalyseWebPage as AnalyseWebPageUseCase;
use Crawl24\App\ValueObject\Heading;
use Crawl24\App\ValueObject\Link;
use Exception;
use Fig\Http\Message\RequestMethodInterface;
use Fig\Http\Message\StatusCodeInterface;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Prophecy\Argument;
use Psr\Http\Message\UriInterface;
use Psr\Log\LoggerInterface;

final class AnalyseWebPageTest extends TestCase
{
    public function test_controller_return_bad_request_when_body_is_empty()
    {
        $logger = $this->prophesize(LoggerInterface::class)->reveal();
        $analyseWebPageUseCase = $this->prophesize(AnalyseWebPageUseCase::class)->reveal();
        $analyseWebPageController = new AnalyseWebPage($analyseWebPageUseCase, $logger);
        $request = new Request(RequestMethodInterface::METHOD_POST, '/');
        $response = new Response();
        $response = $analyseWebPageController->__invoke($request, $response);
        $this->assertSame(StatusCodeInterface::STATUS_BAD_REQUEST, $response->getStatusCode());
    }

    public function test_controller_return_bad_request_when_body_is_invalid()
    {
        $logger = $this->prophesize(LoggerInterface::class);
        $logger->alert(Argument::type('string'), Argument::type('array'))->shouldBeCalledTimes(1);
        $logger = $logger->reveal();
        $analyseWebPageUseCase = $this->prophesize(AnalyseWebPageUseCase::class)->reveal();
        $analyseWebPageController = new AnalyseWebPage($analyseWebPageUseCase, $logger);
        $request = new Request(RequestMethodInterface::METHOD_POST, '/', [], '{"uri":"invalid url"}');
        $response = new Response();
        $response = $analyseWebPageController->__invoke($request, $response);
        $this->assertSame(StatusCodeInterface::STATUS_BAD_REQUEST, $response->getStatusCode());
    }

    public function test_controller_return_use_case_web_page_analyses()
    {
        $webPageAnalyses = $this->factoryFaker->instance(WebPageAnalyses::class);
        $logger = $this->prophesize(LoggerInterface::class)->reveal();
        $analyseWebPageUseCase = $this->prophesize(AnalyseWebPageUseCase::class);
        $analyseWebPageUseCase->__invoke(Argument::type(UriInterface::class))->willReturn($webPageAnalyses);
        $analyseWebPageUseCase = $analyseWebPageUseCase->reveal();
        $analyseWebPageController = new AnalyseWebPage($analyseWebPageUseCase, $logger);
        $request = new Request(RequestMethodInterface::METHOD_POST, '/', [], '{"uri":"http://www.google.com/"}');
        $response = new Response();
        $response = $analyseWebPageController->__invoke($request, $response);
        $mapped = [
            'version' => $webPageAnalyses->HTMLVersion()->version(),
            'page_title' => $webPageAnalyses->pageTitle(),
            'headings' => \array_map(function (Heading $heading) {
                return $heading->sectionLevel();
            }, $webPageAnalyses->headingsCollection()),
            'links' => \array_map(function (Link $link) {
                return \json_encode([
                    'uri' => $link->uri(),
                    'is_reachable' => $link->isReachable(),
                    'is_internal' => $link->isInternal(),
                ]);
            }, $webPageAnalyses->linksCollection()),
            'has_login_form' => $webPageAnalyses->hasLoginForm(),
        ];
        $this->assertSame(StatusCodeInterface::STATUS_OK, $response->getStatusCode());
        $this->assertSame(\json_encode($mapped), $response->getBody()->getContents());
    }

    public function test_controller_log_excpetion_when_case_web_page_analyses_fails()
    {
        $logger = $this->prophesize(LoggerInterface::class);
        $logger->alert(Argument::type('string'), Argument::type('array'))->shouldBeCalledTimes(1);
        $logger = $logger->reveal();
        $analyseWebPageUseCase = $this->prophesize(AnalyseWebPageUseCase::class);
        $analyseWebPageUseCase->__invoke(Argument::type(UriInterface::class))->willThrow(Exception::class);
        $analyseWebPageUseCase = $analyseWebPageUseCase->reveal();
        $analyseWebPageController = new AnalyseWebPage($analyseWebPageUseCase, $logger);
        $request = new Request(RequestMethodInterface::METHOD_POST, '/', [], '{"uri":"http://www.google.com/"}');
        $response = new Response();
        $response = $analyseWebPageController->__invoke($request, $response);
        $this->assertSame(StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR, $response->getStatusCode());
    }
}
