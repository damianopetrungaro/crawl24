<?php

declare(strict_types=1);

use Crawl24\App\Aggregate\WebPageAnalyses;
use Crawl24\App\ValueObject\Heading;
use Crawl24\App\ValueObject\HTMLVersion;
use Crawl24\App\ValueObject\Link;
use League\FactoryMuffin\FactoryMuffin;

$faker = Faker\Factory::create();

/* @var FactoryMuffin $fm */
$fm->define(Faker\Factory::class)->setMaker(function () use ($faker) {
    return $faker;
});

$fm->define(Heading::class)->setMaker(function () use ($faker) {
    return new Heading($faker->numberBetween(1, 6));
});

$fm->define(HTMLVersion::class)->setMaker(function () {
    return HTMLVersion::fromDoctype('<!doctype html>');
});

$fm->define(Link::class)->setMaker(function () use ($faker) {
    return Link::fromString($faker->url, $faker->boolean(), $faker->boolean());
});

$fm->define(WebPageAnalyses::class)->setMaker(function () use ($faker, $fm) {
    return new WebPageAnalyses(
        $fm->instance(HTMLVersion::class),
        $faker->sentence(),
        \array_fill(0, 4, $fm->instance(Heading::class)),
        \array_fill(0, 4, $fm->instance(Link::class)),
        $faker->boolean()
    );
});
