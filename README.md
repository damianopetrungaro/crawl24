# Crawl 24

## What did i do?

#### Preface
I decided to put more effort than required to show my way of writing code, implementing Clean Code and DDD concepts.

#### Design
BTW i do not have unlimited time free, so i have to do some trade-off, and being a test i decided to do not pay much attention to logging and exception handling.

There are some things i may refactor (for example the DomCrawler may accepts some strategies and cover more complex html analyses).

Tests can be also rewritten in a more compact way so to be more clear to the dev (see some huge data provider)

There are 2 Dockerfile one production ready the other one for dev purposes (Next section will explain a little bit).

Components and dependency used are not really important for me, all the dependency have to be easy to change or remove.

The only dependency i `accept` in the Domain layer are PSRs or interface that are not ready as PSRs (ClientInterface is an example).
   
Error messages into Value Object can be moved into a specific exception class and those exception logged by the fw-handler.

Please notice that infrastructure and Application layer has been added only after the Domain layer was defined and ready.

Implementation is pretty easy:
- `src`
    - Aggregate represent the domain model
    - Client to interact with web page to retrieve from the network 
    - Controller is the application layer used to trigger the Domain UseCases
    - DomCrawler the service who analyze the web page
    - Exception contains some `generic-`exceptions 
    - Factory to create complex object that can't be created into the container
    - UseCase, just the domain use cases without fw/application/infrastructure dependency
    - Value Object all the object that represent a value into the Domain
- `bootstrap`
    - Contain the dependency container definition
- `public`
    - Load the fw and the dependencies (http application) 
- `tests` just tests

*NOTE: I AM NOT A FRONTEND DEVELOPER*

This is the first time i write some js and html, the react-app it's pretty easy :D

Feel free to kill me once you'll see no-idiomatic js code :D  

`Beautiful and helpful commit message are used!` - > take a look to the [spec](https://conventionalcommits.org/)

## Build

### Build production ready container
- Build applications: `docker-compose up -d`
- SSH into container: `docker-compose exec crawl24-(api||app||reverse-proxy) bash`

### Build and run development container
- Build image (after production one): `docker-compose -f docker-compose.dev.yml -d`
  if you want you can specify the xDebug remote host file `add the env to the api container XDEBUG_REMOTE_HOST=x.x.x.x`

*IMPORTANT*

Add endpoints to hosts file!

(osx) `echo '127.0.0.1 api.crawl24\n127.0.0.1 crawl24' > /etc/hosts`
